console.log("Hello World")

let firstNumber = parseInt(prompt("Provide a number"));
let secondNumber = parseInt(prompt("Provide another number"));
let total = firstNumber + secondNumber

if (total < 10){	
	console.warn(`The sum of two number is ${total}`)
}
else if (total >= 10 && total <= 20){
	let difference = firstNumber - secondNumber
	alert(`The difference of two number is ${difference}`)
}
else if (total >= 21 && total <= 29){
	let product = firstNumber * secondNumber
	alert(`The product of two number is ${product}`)
}
else if (total >= 30){
	let quotient = firstNumber / secondNumber
	alert(`The quotient of two number is ${quotient}`)
}
else{
	alert("Invalid Number")
};

let name = prompt("What is your first name?")
let age = parseInt(prompt("What is your age?"))
function isLegalAge(){
	if (age >= 18){
		alert("You are of legal age.")
	}
	else if (age <= 17){
		alert("You are not allowed here.")
	}
};

if(name === "" || age === ""){
	alert ("Are you a time traveler?")
	isLegalAge()
}
else if (name !== "" && age !== ""){
	alert (`Hello ${name}. Your age is ${age}.`)
	isLegalAge()
};

switch (age){
	case 18:
		alert("You are not allowed to the party.");
		break;
	case 21:
		alert("You are now part of the adult society.");
		break;
	case 65:
		alert("We thank you for your contribution to the society.")
		break;
	default:
		alert("Are you sure you're not an alien?")
};
/*function isLegalAge(){
	try{
		alert(isLegalAge());
	}
	catch(error){
		console.log(typeof error);
		console.warn(error.message);
	}
	finally{
		isLegalAge
	}
};*/