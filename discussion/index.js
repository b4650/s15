function mod(){
	// return 9+2
	// return 9-2
	// return 9*2
	// return 9/2 = 4r1
	// return 9%2
	let x = 10;
	// return x += 2
	// return x -= 2
	// return x *= 2
	// return x /= 2
	return x%= 2
};

console.log(mod());

let x = 1;
let sum = 1
sum += 1;

console.log(sum)

let z = 1
let increment = ++z

console.log(`Result of pre-increment: ${increment}`);
console.log(`Result of pre-increment: ${z}`);

increment = z++;
console.log(`Result of pre-increment: ${increment}`)
console.log(`Result of pre-increment: ${z}`)

let decrement = --z;
console.log(`Result of pre-decrement: ${decrement}`)
console.log(`Result of pre-decrement: ${z}`)

decrement = z--
console.log(`Result of post-decrement: ${decrement}`)
console.log(`Result of post-decrement: ${z}`)

let juan = "juan";
console.log(1 == 1);
console.log(0 == false);
console.log(juan == "juan");

console.log(1 === true);
console.log(juan === "Juan");

console.log(1 !=1);
console.log(juan != "juan");

let isLegalAge = true;
let isRegistered = true;

allRequirementsMet = isLegalAge && isRegistered;
console.log(`Results of logical AND operator ${allRequirementsMet}`);

allRequirementsMet = isLegalAge || isRegistered
console.log(`Results of logical OR operator ${allRequirementsMet}`);

let num = -1;
if (num<0) {
	console.log("Hello")
}

let value = 30;
if (value > 10){
	console.log("Welcome to Zuitt")
};

num = 5
if ( num > 10 ) {
	console.log(`Number is greater than 10`)
}
else{
	console.log(`Number is less than 10`)
};

/*num = parseInt(prompt("Please input a number."));
if (num>59){
	alert("Senior Age")
	console.log(num)
}
else{
	alert("Invalid Age")
	console.log(num)
};

let city = parseInt(prompt("Enter a Number"));
if (city === 1) {
	alert("Welcome to Quezon City")
}
else if (city === 2){
	alert("Welcome to Valenzuela City")
}
else if (city === 3){
	alert("Welcome to Pasig City")
}
else if (city === 4){
	alert("Welcome to Taguig City")
}
else{
	alert("Invalid Number")
};*/

let message = "";

function determineTyphoonIntensity(windspeed){
	if(windspeed<30){
		return `Not a typhoon yet.`
	}
	else if (windspeed <= 61){
		return `Tropical Depression detected`
	}
	else if (windspeed >= 62 && windspeed <= 88){
		return `Tropical Storm detected.`
	}
	else if(windspeed >=89 && windspeed <= 117){
		return `Sever Tropical Storm detected.`
	}
	else{
		return `Typhoon detected`
	}
}
message = determineTyphoonIntensity(120);
console.log(message)

let ternaryResult = (1 < 18) ? true : false
console.log(`Result of ternary operator: ${ternaryResult}`);

/*let name;
function isOfLegalAge(){
	name = "John"
	return "You are of the legal age Limit"
};

function isUnderAge(){
	name = "Jane";
	return "You are under the legal Age"
};

let age = parseInt(prompt("What is your age?"));
let LegalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
alert(`Result of Ternary Operator in Function ${LegalAge} ${name}`);*/

/*let day = prompt("What day is it today?").toLowerCase();
switch (day){
	case "sunday":
		alert("The color of the day is red");
		break;
	case "monday":
		alert("The color of the day is orange");
		break;
	case "tuesday":
		alert("The color of the day is yellow");
		break;
	case "wednesday":
		alert("The color of the day is green");
		break;
	case "thursday":
		alert("The color of the day is blue");
		break;
	case "friday":
		alert("The color of the day is violet");
		break;
	case "saturday":
		alert("The color of the day is indigo");
		break;
	default:
		alert("Please input a valid day");	
}*/

function showIntensityAlert(windspeed){
	try{
		alert(determineTyphoonIntensity(windspeed))
	}
	catch(error){
		console.log(typeof error);
		console.log(error.message);
	}
	finally{
		alert("Intensity updates will show new alert")
	}
}
showIntensityAlert(56);